<?php

namespace Shopware\SisDeactivateProductWhenNoInstock\Commands;

use Shopware\Components\Model\ModelManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Shopware\Commands\ShopwareCommand;
use ClassForPluginCommand;

use Shopware\SisDeactivateProductWhenNoInstock\Components\PluginFunction;

class DeactivateProductWhenNoInstock extends ShopwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('sis:articles:deactivate')
            ->setDescription('Description of this command')
            ->addOption(
                'SAOZ',
          null,
                InputOption::VALUE_NONE,
                'SetActiveOfZero'
            )
            ->addOption(
                'SVATZ',
            null,
                InputOption::VALUE_NONE,
                'SetVariantArticleToZero'
            )
            ->addOption(
                'XMA',
            null,
                InputOption::VALUE_NONE,
                'ChangeMainArticle'
            )


            ->setHelp(<<<EOF
The <info>%command.name%</info> implements a command ceck.
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    public $PluginFunction;
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $PluginFunction = new PluginFunction();
        if ($input->getOption('SAOZ')){
            $PluginFunction->SetActiveOfZero();
        }
        if ($input->getOption('SVATZ')){
            $PluginFunction->SetVariantArticleToZero();
        }
        if ($input->getOption('XMA')) {
            $PluginFunction->ChangeMainArticle();
        }
    }


}
