<?php
//require_once __DIR__ . DIRECTORY_SEPARATOR . 'tester.php';
namespace Shopware\SisDeactivateProductWhenNoInstock\Tests;

use Shopware\Components\Test\Plugin\TestCase;

class PersonTest extends TestCase
{
 public $test;
    public function setUp(){
        $this->test =  new Person ("Jason");
    }
    public function testName(){
        $jason = $this->test->getName();
        $this->assertTrue($jason == 'Jason');
    }
}
?>