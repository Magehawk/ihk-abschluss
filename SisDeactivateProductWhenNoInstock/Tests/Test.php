<?php

class PluginTest extends \Shopware\Components\Test\Plugin\TestCase
{
    protected static $ensureLoadedPlugins = array(
        'SisDeactivateProductWhenNoInstock' => array(
        )
    );

    public function setUp()
    {
        parent::setUp();

        $helper = \TestHelper::Instance();
        $loader = $helper->Loader();


        $pluginDir = getcwd() . '/../';

        $loader->registerNamespace(
            'Shopware\\SisDeactivateProductWhenNoInstock',
            $pluginDir
        );
    }

    public function testCanCreateInstance()
    {
        /** @var Shopware_Plugins_Frontend_SisDeactivateProductWhenNoInstock_Bootstrap $plugin */
        $plugin = Shopware()->Plugins()->Frontend()->SisDeactivateProductWhenNoInstock();

        $this->assertInstanceOf('Shopware_Plugins_Frontend_SisDeactivateProductWhenNoInstock_Bootstrap', $plugin);
    }

    //TODO implement the relevant unittests as methods
    public function testCanCreatePerson(){
        $person = new \Shopware\SisDeactivateProductWhenNoInstock\Components\Person("Bjoern");
        $this->assertTrue($person->getName(),'Bjoern');
    }
}