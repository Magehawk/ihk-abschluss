<?php

namespace Shopware\SisDeactivateProductWhenNoInstock\Subscriber;

use Enlight\Event\SubscriberInterface;

class ControllerPath implements SubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
                        'Enlight_Controller_Dispatcher_ControllerPath_Frontend_SisDeactivateProductWhenNoInstock' => 'onGetControllerPathFrontend',        );
    }




    /**
     * Register the frontend controller
     *
     * @param   \Enlight_Event_EventArgs $args
     * @return  string
     * @Enlight\Event Enlight_Controller_Dispatcher_ControllerPath_Frontend_SisDeactivateProductWhenNoInstock     */
    public function onGetControllerPathFrontend(\Enlight_Event_EventArgs $args)
    {
        return __DIR__ . '/../Controllers/Frontend/SisDeactivateProductWhenNoInstock.php';
    }
}
