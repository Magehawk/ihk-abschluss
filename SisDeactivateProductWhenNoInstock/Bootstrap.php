<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Shopware\SisDeactivateProductWhenNoInstock\Components\PluginFunction;
/**
 * The Bootstrap class is the main entry point of any shopware plugin.
 *
 * Short function reference
 * - install: Called a single time during (re)installation. Here you can trigger install-time actions like
 *   - creating the menu
 *   - creating attributes
 *   - creating database tables
 *   You need to return "true" or array('success' => true, 'invalidateCache' => array()) in order to let the installation
 *   be successfull
 *
 * - update: Triggered when the user updates the plugin. You will get passes the former version of the plugin as param
 *   In order to let the update be successful, return "true"
 *
 * - uninstall: Triggered when the plugin is reinstalled or uninstalled. Clean up your tables here.
 */
class Shopware_Plugins_Frontend_SisDeactivateProductWhenNoInstock_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    public function getVersion() {
        $info = json_decode(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .'plugin.json'), true);
        if ($info) {
            return $info['currentVersion'];
        } else {
            throw new Exception('The plugin has an invalid version file.');
        }
    }

    public function getLabel()
    {
        return 'SisDeactivateProductWhenNoInstock';
    }

    public function uninstall()
    {
        return true;
    }

    public function update($oldVersion)
    {
        return true;
    }

    public function install()
    {
        if (!$this->assertMinimumVersion('4.3.0')) {
            throw new \RuntimeException('At least Shopware 4.3.0 is required');
        }
        $this->registerCronJobs();

        $this->subscribeEvent(
            'Enlight_Controller_Front_DispatchLoopStartup',
            'onStartDispatch'
        );

        $this->subscribeEvent(
            'Shopware_Console_Add_Command',
            'onAddConsoleCommand'
        );
        $this->InstallConfiguration();

        return array('success' => true, 'invalidateCache' => array('frontend', 'backend'));

    }
    private function registerCronJobs()
    {
        $this->createCronJob(
            'SisDeactivateProductWhenNoInstock',
            'DeactivateArticle',
            3600,
            true
        );
        $this->subscribeEvent(
            'Shopware_CronJob_DeactivateArticle',
            'OnDeactivateArticle'
        );
    }

    /**
     * Callback function of the console event subscriber. Register your console commands here.
     */
    public function onAddConsoleCommand(Enlight_Event_EventArgs $args)
    {
        $this->registerMyComponents();
        // You can easily add more commands here
          return new \Doctrine\Common\Collections\ArrayCollection(array(
               new \Shopware\SisDeactivateProductWhenNoInstock\Commands\DeactivateProductWhenNoInstock()
           ));
    }

    /**
     * This callback function is triggered at the very beginning of the dispatch process and allows
     * us to register additional events on the fly. This way you won't ever need to reinstall you
     * plugin for new events - any event and hook can simply be registerend in the event subscribers
     */
    public function onStartDispatch(Enlight_Event_EventArgs $args)
    {
        $this->registerMyComponents();
                        $this->registerMyTemplateDir();
        $this->registerMySnippets();


        $subscribers = array(
            new \Shopware\SisDeactivateProductWhenNoInstock\Subscriber\ControllerPath(),
            new \Shopware\SisDeactivateProductWhenNoInstock\Subscriber\Frontend()
        );

        foreach ($subscribers as $subscriber) {
            $this->Application()->Events()->addSubscriber($subscriber);
        }
    }

    /**
     * Registers the template directory, needed for templates in frontend an backend
     */
    public function registerMyTemplateDir()
    {
        Shopware()->Template()->addTemplateDir($this->Path() . 'Views');
    }

    /**
     * Registers the snippet directory, needed for backend snippets
     */
    public function registerMySnippets()
    {
        $this->Application()->Snippets()->addConfigDir(
            $this->Path() . 'Snippets/'
        );
    }

    public function registerMyComponents()
    {
        $this->Application()->Loader()->registerNamespace(
            'Shopware\SisDeactivateProductWhenNoInstock',
            $this->Path()
        );
    }
    /**
     * Plugin Content
     */
    public function InstallConfiguration()
    {
        $form = $this->Form();
        $form->setElement('select', 'SetActiveOfZero', array(
            'label' => 'setzt aktive auf Null',
            'store' => array(
                array(
                    1,
                    'Aktiv'
                ),
                array(
                    0,
                    'Deaktiviert'
                )
            )
        ));
        $form->setElement('select', 'SetVariantArticletoZero', array(
            'label' => 'deaktivert Variantartikel',
            'store' => array(
                array(
                    1,
                    'Aktiv'
                ),
                array(
                    0,
                    'Deaktiviert'
                )
            )
        ));
        $form->setElement('select', 'ChangeMainArticle', array(
                'label' => 'Wechselt Stammartikel',
                'store' => array(
                    array(
                        1,
                        'Aktiv'
                    ),
                    array(
                        0,
                        'Deaktiviert'
                    )
                )
            ));

    }
    public function OnDeactivateArticle()
    {
        $PluginFunction = new PluginFunction();
        if ($this->Config()->SetActiveOfZero == 1) {
            $PluginFunction->SetActiveOfZero();
        }
        if ($this->Config()->SetVariantArticletoZero == 1) {
            $PluginFunction->SetVariantArticletoZero();
        }
        if ($this->Config()->ChangeMainArticle == 1) {
            $PluginFunction->SetVariantArticletoZero();
        }
    }

    public function afterInit()
    {
        $this->registerNamespaces();
    }
    public function registerNamespaces()
    {
        $this->Application()->Loader()->registerNamespace(
            'ShopwarePlugins\\SisDeactivateProductWhenNoInstock\\Components',
            __DIR__ . '/Components/'
        );
    }

}

