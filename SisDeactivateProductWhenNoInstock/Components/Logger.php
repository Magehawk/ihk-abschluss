<?php
namespace Shopware\SisDeactivateProductWhenNoInstock\Components;
class Logger
{
    private $filePath;
    private $auth;
    private $username;
    private $date;
    private $article;

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    /**
     * @return bool|string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param bool|string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Logger constructor.
     * @param $filePath
     * @param $auth
     * @param $username
     */
    public function __construct()
    {
        $this->filePath = __DIR__ . "/Logs/Logs.txt";
        $this->date = date('Y-m-d G:i:s');
    }
    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
    /**
     * @return mixed
     */
    public function getAuth()
    {
        return $this->auth;
    }
    /**
     * @param mixed $auth
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;
    }
    /*
    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }
    /**
     * @param mixed $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }
    public function logger($info)
    {
        $content =  $this->date . ": " . $info .  "\n";
        file_put_contents($this->filePath, $content, FILE_APPEND);
    }
}


