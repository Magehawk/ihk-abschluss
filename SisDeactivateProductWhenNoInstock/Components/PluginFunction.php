<?php
/**
 * Created by PhpStorm.
 * User: n.zimmermann
 * Date: 20.05.2016
 * Time: 22:31
 */
namespace Shopware\SisDeactivateProductWhenNoInstock\Components;
 class PluginFunction
{
    public function SetActiveOfZero()
    {
        $qb = Shopware()->Models()->createQueryBuilder();
        $qb->select(array('details'))
            ->from('Shopware\Models\Article\Detail', 'details')
            ->innerJoin('details.article', 'article')
            ->where('details.inStock = :instock')
            ->andWhere('details.active = :active')
            ->andWhere('details.kind = :kind')
            ->andWhere('article.lastStock = :laststock')
            ->setParameter('instock', 0)
            ->setParameter('active', 1)
            ->setParameter('kind', 1)
            ->setParameter('laststock', 1);
        $query = $qb->getQuery();
        $result = $query->getResult();
        foreach ($result as $article) {
            $article->setActive(0);
            Shopware()->Models()->persist($article);
            Shopware()->Models()->flush();
            $this->TrackArticle($article);
        }

    }

    public function SetVariantArticleToZero()
    {
        $qb = Shopware()->Models()->createQueryBuilder();
        $qb->select(array('details'))
            ->from('Shopware\Models\Article\Detail', 'details')
            ->innerJoin('details.article', 'article')
            ->where('details.inStock = :instock')
            ->andWhere('details.active = :active')
            ->andWhere('details.kind = :kind')
            ->andWhere('article.lastStock = :laststock')
            ->setParameter('instock', 0)
            ->setParameter('active', 1)
            ->setParameter('kind', 2)
            ->setParameter('laststock', 1);
        $query = $qb->getQuery();
        $result = $query->getResult();
        foreach ($result as $article) {
            $article->setActive(0);
            Shopware()->Models()->persist($article);
            Shopware()->Models()->flush();
            $this->TrackArticle($article);
        }
    }

    public function ChangeMainArticle()
    {
        $qb = Shopware()->Models()->createQueryBuilder();
        $qb->select(array('details'))
            ->from('Shopware\Models\Article\Detail', 'details')
            ->innerJoin('details.article', 'article')
            ->where('details.inStock <= :instock')
            ->andWhere('details.active = :active')
            ->andWhere('details.kind = :kind')
            ->andWhere('article.lastStock = :laststock')
            ->setParameter('instock', 0)
            ->setParameter('active', 1)
            ->setParameter('kind', 1)
            ->setParameter('laststock', 1);
        $query = $qb->getQuery();
        $result = $query->getResult();
        foreach ($result as $articleDetails) {
            $qb = Shopware()->Models()->createQueryBuilder();
            $qb->select(array('details'))
                ->from('Shopware\Models\Article\Detail', 'details')
                ->where('details.article = :articleID')
                ->andWhere('details.active = :active')
                ->andWhere('details.kind = :kind')
                ->andWhere('details.inStock >= :instock')
                ->setParameter('articleID', $articleDetails->getArticleId())
                ->setParameter('instock', 1)
                ->setParameter('active', 1)
                ->setParameter('kind', 2);
            $query = $qb->getQuery();
            $totalRows = $query->getResult();
            if ($totalRows) {
                foreach ($totalRows as $article2) {
                    $article2->setKind(1);
                    Shopware()->Models()->persist($article2);
                    Shopware()->Models()->flush();
                    $this->TrackArticle($article2);
                    break;
                }
                $articleDetails->setKind(2);
            }
            $articleDetails->setActive(0);
            Shopware()->Models()->persist($articleDetails);
            Shopware()->Models()->flush();
            $this->TrackArticle($articleDetails);
        }
    }
     public function TrackArticle(\Shopware\Models\Article\Detail $article)
     {
         $logger= new \Shopware\SisDeactivateProductWhenNoInstock\Components\Logger();
         $logger->logger($article->getNumber());
     }
}